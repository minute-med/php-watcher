#!/bin/bash


RED="\e[31m"
GREEN="\e[32m"
NC="\e[0m" # No Color

check_xdotool() {

	if ! xdotool_loc="$(type -P xdotool &>/dev/null)"; then
	  echo -e "${RED}xdotool:Not installed${NC}"
	  echo 'aborting'
	  exit 1
	else
	  echo -e "${GREEN}xdotool:Installed${NC}"
	fi
}

check_php() {

	if ! php_loc="$(type -P php &>/dev/null)"; then
	  echo -e "${RED}php:Not installed${NC}"
	  echo 'aborting'
	  exit 1
	else
	  echo -e "${GREEN}php:Installed${NC}"
	fi

}



check_php_version() {

    # The current PHP Version of the machine
    PHPVersion=$(php -v|grep --only-matching --perl-regexp "\\d+\.\\d+\.\\d+");
    currentVersion=${PHPVersion:0:3};
    
    minimumRequiredVersion='5.3';
    if ! [ $(echo " $currentVersion >= $minimumRequiredVersion" | bc) -eq 1 ]; then
        echo -e "${RED}PHP Version < 5.3${NC}"
	  	echo 'aborting'
        exit 1
    else
        echo -e "${GREEN}PHP Version => 5.3${NC}"
    fi

}

check_inotify() {

	inotify_loaded=$(php -m | grep inotify | head -1)

	if test ! ${#inotify_loaded} -gt 0;then
		echo -e "${RED}inotify not loaded${NC}"
	  	echo 'aborting'
	  	exit 1
	else
		echo -e "${GREEN}inotify loaded${NC}"
 	fi
}

check_xdotool
check_php
check_php_version
check_inotify

echo "All requirements OK, installing..."

path_to_cmd=`pwd`
flag="#php-watcher_flag (do not remove this comment)"
line="PATH=\"$path_to_cmd:"'$PATH"'

# if there isn't the flag.
if ! found_flag=$(find "$HOME/.profile" -type f -print0 | xargs -0 grep "$flag"); then
	# echo "exporting path to ${HOME}/.profile"
	echo -e "\n$flag" >> "$HOME/.profile"
	echo "$line" >> "$HOME/.profile"
else
	# echo 'updating path in ${HOME}/.profile'
	line_number=$(awk "/#php-watcher_flag/{ print NR; exit }" $HOME/.profile)
	line_number=$((line_number + 1))
	sed -i ${line_number}'s/.*//g' "$HOME/.profile"
	echo -ne "$line" >> "$HOME/.profile"
	source "$HOME/.profile"
fi
# export path variable