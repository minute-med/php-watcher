#!/bin/bash
#
# L Nix <lornix@lornix.com>
# reload browser window
#
# whether to use SHIFT+CTRL+R to force reload without cache
#RELOAD_KEYS="CTRL+R"
RELOAD_KEYS="SHIFT+CTRL+R"
#
# set to whatever's given as argument
BROWSER=$1
#
# if was empty, default set to name of browser, firefox/chrome/opera/etc..
if [ -z "${BROWSER}" ]; then
    BROWSER=google-chrome
fi

# MYWINDOW=$(xdotool search --desktop 0 "Mozilla Firefox" | head -1)
WD=$(xdotool search --desktop 0 --onlyvisible --class ${BROWSER}|head -1)

xdotool windowfocus --sync ${WD}
xdotool windowactivate --sync ${WD}
xdotool key --clearmodifiers ${RELOAD_KEYS}