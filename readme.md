# PHP-Watcher

### Introduction

php-watch is a CLI php script that watch file modification for provided file/folder and refresh
the web browser active tab.

**note**
> There can be a weird behaviour that stuck the window(cannot Alt+Tab or click taskbar tabs) after the browser reload.
Just press `Ctrl` key once and everything will be in order

**supported browser**

 * google chrome
 * mozilla firefox
 * opera

### Dependencies

 * php => 5.3
 * PECL inotify => 0.1.2 (php extension 'inotify')
 * xdotool

### Installation

configure script will check if the dependencies are installed
if yes, it will edit ~/.profile to add php-watch script location to PATH env variable

```bash
./configure.sh
```

### How to use

```bash
php-watch path/to/file/or/folder
```

if you want to run it as background process and keep hand over the term, you can add & at the end of the command

```bash
php-watch path/to/file/or/folder &
```

## TODO

* select target browser as script argument (eg: --target=google-chrome)
* watch multiple file/directory
* verose logs (what file has been saved and which url has been reloaded)